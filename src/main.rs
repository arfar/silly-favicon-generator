extern crate bit_vec;
extern crate crypto;
extern crate raster;

use self::crypto::digest::Digest;
use self::crypto::sha2::Sha256;
use bit_vec::BitVec;
use image::{ImageBuffer, Rgb};
use raster::Color;
use std::env;

fn generate_image(
    string: &str,
    first_colour_string: Option<&str>,
    second_colour_string: Option<&str>,
) {
    let mut image = ImageBuffer::new(16, 16);

    let mut hasher = Sha256::new();
    let mut buffer = [0u8; 32];
    hasher.input_str(string);
    hasher.result(&mut buffer[..]);

    /* This probably isn't the best way to do this, eh */
    let colour_1 = match first_colour_string {
        Some(s) => Color::hex(s).unwrap(),
        None => Color::hex("#336699").unwrap(),
    };
    let colour_1 = Rgb([colour_1.r, colour_1.g, colour_1.b]);

    let colour_2 = match second_colour_string {
        Some(s) => Color::hex(s).unwrap(),
        None => Color::hex("#000000").unwrap(),
    };
    let colour_2 = Rgb([colour_2.r, colour_2.g, colour_2.b]);

    let bv = BitVec::from_bytes(&buffer);

    for i in 0..16 * 16 as u32 {
        match bv[i as usize] {
            true => image.put_pixel(i % 16, i / 16, colour_1),
            false => image.put_pixel(i % 16, i / 16, colour_2),
        }
    }
    image.save("output.png").unwrap();
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() <= 1 || args.len() >= 5 {
        println!("USAGE:");
        println!("\tsilly_favicon_generator string [COLOUR IN HEX] [COLOUR IN HEX]");
        println!("\tNote, you gotta put the '#' before the number");
        return ();
    }

    /* I'm sure there's a better way to do this */
    if args.len() == 2 {
        generate_image(&args[1], None, None);
    }
    if args.len() == 3 {
        generate_image(&args[1], Some(&args[2]), None);
    }
    if args.len() == 4 {
        generate_image(&args[1], Some(&args[2]), Some(&args[3]));
    }
}
